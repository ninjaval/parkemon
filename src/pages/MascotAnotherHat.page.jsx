import React from 'react'
import Button from "../components/Button.component"
import Content from "../components/Content.component";
import Footer from "../components/Footer.component";
import Header from "../components/Header.component";
import Mascot from "../components/Mascot.component";
import segmentSwitch from "../assets/icons/segmentSwitch.png";

import hat1 from "../assets/icons/hat1.png"
import hat2 from "../assets/icons/hat12.png"
import hat3 from "../assets/icons/hat22.png"
import hat4 from "../assets/icons/hat4.png"

import coints from "../assets/icons/coins.png"

import "./Mascot.styles.scss";
import { useHistory } from "react-router-dom";

export default function MascotAnotherHat() {
    const history = useHistory()
  return (
    <div className="mascot" style={{ backgroundColor: "#F85E08", minHeight: "100vh" }}>
      <div className="container d-flex flex-column" style={{ minHeight: "100vh" }}>
        <Header title="Бонусы"></Header>
        <Content>
         <img alt="switch" src={segmentSwitch} onClick={() => history.push("/mascotBonuses")}/>
          <Mascot type={"mascotWithAnotherHat"} style={{ width: "220px", height: "340px", paddingTop: "10px" }} />
          <div className="hats-grid d-flex justify-content-center mt-3">
            <img src={hat1} alt="123"/>
            <img src={hat2} alt="123"/>
            <img src={hat3} alt="123" onClick={() => history.push("/mascot")}/>
            <img src={hat4} alt="123"/>
          </div>
        </Content>
        <Footer height="100px">
          <div className="d-flex justify-content-between align-items-center h-100">
            <Button text="Потратить бонусы" style={{ backgroundColor: "black", color: "white", width: "70%" }}/>
            <img src={coints} alt="123" style={{ width: "45px", height: "45px"}}/>
          </div>
        </Footer>
      </div>
    </div>
  );
}
