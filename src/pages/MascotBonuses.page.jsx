import React from "react";
import Button from "../components/Button.component"
import Content from "../components/Content.component";
import Footer from "../components/Footer.component";
import Header from "../components/Header.component";
import Bonus from "../components/Bonus.component"

import segmentSwitch from "../assets/icons/segmentSwitch2.png"
import coints from "../assets/icons/coins.png"

import "./Mascot.styles.scss";
import { useHistory } from "react-router-dom";

function MascotBonuses() {
  const history = useHistory()
  return (
    <div className="mascot" style={{ backgroundColor: "#F85E08", minHeight: "100vh" }}>
      <div className="container d-flex flex-column" style={{ minHeight: "100vh" }}>
        <Header title="Бонусы"></Header>
        <Content>
         <img alt="switch" src={segmentSwitch} className="mb-3" onClick={() => history.push("/mascot")}/>
         <Bonus text="Сахарная вата 1 шт." price="100"/>
         <Bonus text="Рожок мороженного" price="150"/>
         <Bonus text="Скидка на любой атракцион 50%" price="250"/>
         <Bonus text="Скидка 30% на любой десерт на фудкорте" price="300"/>
        </Content>
        <Footer height="100px">
          <div className="d-flex justify-content-between align-items-center h-100">
            <Button text="Потратить бонусы" style={{ backgroundColor: "black", color: "white", width: "70%" }}/>
            <img src={coints} alt="123" style={{ width: "45px", height: "45px"}}/>
          </div>
        </Footer>
      </div>
    </div>
  );
}

export default MascotBonuses;
