import React from 'react'
import Button from "../components/Button.component";
import Content from "../components/Content.component";
import Footer from "../components/Footer.component";
import Header from "../components/Header.component";
import Mascot from "../components/Mascot.component";
import Message from "../components/Message.component";

import { useHistory } from "react-router-dom";

const msg = `Тебе понравился атракцион? А хочешь получить бонус?

Тогда поделись фотографией из парка в соц сети и забирай свою скидку!`

export default function QuestPage() {
    const history = useHistory()
    return (
        <div className="quest-page" style={{ backgroundColor: "#08695D"}}>
          <div className="container d-flex flex-column" style={{ minHeight: "100vh" }}>
            <Header title="Задание для тебя"></Header>
            <Content>
                <Message text={msg} rectanglePosition="right"/>
                <Mascot type={"mascot"} style={{ width: "220px", height: "auto", marginTop: "30px" }}/>
            </Content>
            <Footer height="140px">
                <div>
                    <Button text="Хочу" onClick={() => history.push("/mascot")}/>
                    <Button text="Пропустить" style={{ backgroundColor: "black", color: "white" }}/>
                </div>
            </Footer>
          </div>
        </div>
      );
}
