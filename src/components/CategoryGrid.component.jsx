import React from 'react'
import { useHistory } from 'react-router-dom'
import Category from './Category.component'


export default function CategoryGrid() {
    const history = useHistory()
    return (
        <div style={{ display: "flex", flexWrap: "wrap" }}>
            <Category type="attractions" onClick={() => {window.AndroidFunction && window.AndroidFunction.openAtractionsPage()}}/>
            <Category type="interests" onClick={() => {window.AndroidFunction && window.AndroidFunction.openInterestsPage()}}/>
            <Category type="personalCollections" onClick={() => {window.AndroidFunction && window.AndroidFunction.openPersonalCollectionsPage()}}/>
            <Category type="foodcorts" onClick={() => {window.AndroidFunction && window.AndroidFunction.openFoodPage()}}/>
            <Category type="souvenirs" onClick={() => {window.AndroidFunction && window.AndroidFunction.openSouvenirsPage()}}/>
            <Category type="bonuses" onClick={() => history.push("/mascot")}/>
            <Category type="replyUs" onClick={() => {window.AndroidFunction && window.AndroidFunction.openReplyUsPage()}}/>
        </div>
    )
}
