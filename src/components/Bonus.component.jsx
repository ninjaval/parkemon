import React from 'react'

export default function Bonus({ price, text}) {
    return (
        <div className="d-flex justify-content-between my-2 p-3 align-items-center" style={{ 
            backgroundColor: "#FFBD12", 
            minHeight: "50px",
            borderRadius: "20px",
            border: "2px solid #18191F"
        }}>
            <p className="mb-0" style={{ fontWeight: 800 }}>{text}</p>
            <p className="mb-0" style={{ fontWeight: 800, fontSize: "28px" }}>{price}</p>
        </div>
    )
}
