import React from "react";
import "./Header.styles.scss";

import back from "../assets/icons/back.png";

import notificationButton from "../assets/icons/notificationButton.png";
import { useHistory } from "react-router-dom";

function Header({ title }) {
  const history = useHistory();
  return (
    <div className="header d-flex justify-content-between align-items-center p-2">
      <div className="row">
        <img
          alt="back"
          className="mx-2"
          style={{ width: "35px", height: "35px" }}
          src={back}
          onClick={() => history.push("/categories")}
        />
        <div style={{ fontWeight: 800, color: "white", fontSize: "27px" }}>
          {title}
        </div>
      </div>
      <img
        src={notificationButton}
        alt="mascot img"
        style={{ width: "48px", height: "48px" }}
      />
    </div>
  );
}

export default Header;
