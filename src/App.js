import React from 'react';
import './App.css';
import WelcomePage from './pages/Welcome.page';
import Activities from './pages/Activities.page';
import QuestPage from './pages/Quest.page';
import { Route, Switch } from "react-router-dom"
import MascotPage from './pages/Mascot.page';
import MascotWithAnotherHatPage from "./pages/MascotAnotherHat.page"
import MascotBonuses from "./pages/MascotBonuses.page"

function App() {
  return (
    <div className="">
      <Switch>
        <Route exact path="/" component={WelcomePage}/>
        <Route path="/categories" component={Activities}/>
        <Route path="/quest" component={QuestPage}/>
        <Route exact path="/mascot" component={MascotPage}/>
        <Route exact path="/mascotWithAnotherHat" component={MascotWithAnotherHatPage}/>
        <Route path="/mascotBonuses" component={MascotBonuses}/>
      </Switch>
    </div>
  );
}

export default App;
